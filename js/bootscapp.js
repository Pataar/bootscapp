var module = angular.module('ngBootscapp', ['mongolabResource']);

module.constant('API_KEY', 'Y963a6nqWM8QgFUZ1JiocS9u5dVIcH9S');
module.constant('DB_NAME', 'bootscapp');

module.controller('ngNavigationController', function ($scope, $rootScope, $location) {
	//init active
	$scope.menu = [
		{
			key: 'boodschappen',
			value: "Boodschappen",
			useCounter: false
		},
		{
			key: 'snacks',
			value: "Snacks",
			useCounter: true
		},
		{
			key: 'likkepot',
			value: "Likkepot",
			useCounter: true
		},
		{
			key: 'settings',
			value: 'Settings'
		}
	];

	$scope.setActive = function (item) {
		$rootScope.settings = (item.key == "settings");
		$rootScope.active = item;
		$location.hash(item.key);
		angular.element(".collapse.in.navbar-collapse").collapse('hide');
	};

	if($location.hash()){
		for(i in $scope.menu){
			if($scope.menu[i].key == $location.hash()){
				$scope.setActive($scope.menu[i]);
				break;
			}
		}
	} else {
		$scope.setActive($scope.menu[0]);
	}
});

module.controller('ngSettingsController', function ($scope, $rootScope, $mongolabResource, $location) {

	$rootScope.saveUser = function () {
		$rootScope.user.id = btoa($rootScope.user.name);
		localStorage.user = JSON.stringify($rootScope.user);
	};

	if (localStorage.user) {
		$rootScope.user = JSON.parse(localStorage.user);
	} else {
		$rootScope.user = {};
	}

});

module.controller('ngBootscappController', function($scope, $rootScope, $timeout, $mongolabResource){
	var to = {};
	var config = {
		refreshtime : 10
	};

	$scope.addFood = function(name){
		if(name && name.trim() != ""){
			name = name.charAt(0).toUpperCase() + name.slice(1);
			$scope.FoodCollection.add(name);
			$scope.foodToAdd = "";
			$scope.autocomplete = false;
		}
	};

	$rootScope.$watch('active', function() {
		if($rootScope.active.key != 'settings')
		$scope.getFoodCollection();
	});

	$scope.getFoodCollection = function(){
		$scope.FoodCollection.init($rootScope.active);
		$timeout.cancel(to);
		syncloop();
	};



	$scope.FoodCollection = {

		coll: {},
		useCounter: false,
		key: '',
		resource: {},


		init: function(active){
			this.key = active.key;
			this.useCounter = active.useCounter;

			this.resource = $mongolabResource(this.key);

			var json;

			if(json = localStorage[this.key]){
				this.coll = JSON.parse(json);
			}

			var me = this;

			this.resource.query(function(data){
				me.coll = data;
				localStorage[me.key] = JSON.stringify(me.coll);
			});

			return this;
		},

		add: function (name) {
			var food = {};
			food._id = btoa(name);

			var me = this;
			var existingFood = this.get(food._id);

			if (existingFood && existingFood.count > 0) {
				//food exists

				existingFood.users.push($rootScope.user);
				me.save(me.count(existingFood, 1));
			} else {
				//food does not exist
				food.key = "";
				food.name = name;
				food.count = 1;
				food.totalCount = 1;
				food.crossed = false;
				food.users = [$rootScope.user];
				me.coll.push(food);
				me.save(food);
			}

			return this;
		},

		count: function(data, change){
			data.count += change;
			data.totalCount += change;
			return data;
		},

		get: function (id) {
			for(var i in this.coll){
				if(this.coll[i]._id == id){
					return this.coll[i];
				}
			}
			return false;
		},

		save: function(value){
			this.resource.save(value);
			return this;
		},

		cross: function (food) {
			var me = this;
			food.crossed = !food.crossed;
			me.save(food);
			return this;
		},

		sync: function () {
			var me = this;
			$scope.sync = true;
			this.resource.query(function (data) {
				me.coll = data;
				localStorage[me.key] = JSON.stringify(me.coll);
				$scope.sync = false;
				var d = (new Date());
				$scope.syncDate = ((d.getHours() < 10) ? '0'+d.getHours() : d.getHours()) + ':' + ((d.getMinutes() < 10) ? '0'+d.getMinutes() : d.getMinutes())+ ':' +
					((d.getSeconds() < 10) ? '0'+d.getSeconds() : d.getSeconds());
			}, function(){
				$scope.sync = false;

			});
		},
		clear: function(){
			for(var i in this.coll){
				this.remove(this.coll[i]);
			}
			localStorage.removeItem(this.key);
		},

		remove: function(food){
			food.count = 0;
			food.crossed = false;
			food.users = [];
			this.save(food);
		}



	};


	var syncloop = function () {
		$scope.FoodCollection.sync();
		to = $timeout(syncloop, config.refreshtime*1000);
	};


	$scope.closeAutoComplete = function(){
		$timeout(function(){
			$scope.autocomplete = false;
		}, 100);
	};

	$scope.toggleMenu = function(id){
		if($rootScope.open == id){
			$rootScope.open = false;
		} else {
			$rootScope.open = id;
		}
	}

});

module.filter('array', function() {
	return function(items, a) {

		var filtered = [];
		angular.forEach(items, function(item) {
			//if item has been chosen multiple times
			if(item.totalCount > 1 && a && a.length > 0)
				filtered.push(item);
		});
		return filtered;
	};
});




// This is a module for cloud persistance in mongolab - https://mongolab.com
angular.module('mongolabResource', ['ngResource']).factory('$mongolabResource', ['$resource', 'API_KEY', 'DB_NAME', function ($resource, API_KEY, DB_NAME) {

	function MmongolabResourceFactory(collectionName) {

		var resource = $resource('https://api.mongolab.com/api/1/databases/' + DB_NAME + '/collections/' + collectionName + '/:id',
			{ apiKey:API_KEY, id:'@_id.$oid'}, { update:{ method:'PUT' } }
		);

		resource.getById = function (id, cb, errorcb) {
			return resource.get({id:id}, cb, errorcb);
		};

		return resource;
	}

	return MmongolabResourceFactory;
}]);